       IDENTIFICATION DIVISION. 
       PROGRAM-ID.  ConditionNames.
       AUTHOR.   Michael Coughlan.
           * Using condition names (level 88's) and the EVALUATE
       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01 CharIn          PIC X.
           88 Vowel       VALUE "a","e","i","o","u".
           